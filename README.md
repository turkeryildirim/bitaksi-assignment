## Start
```
mvn spring-boot:run
```

### Credentials
```
TestUser:test123
```

## Endpoints

getAllTrips:
```
/trips?start=31.17821068,-97.38887025&radius=5
```

getAllTripsByStartAndCompleteDate:
```
/trips?start=31.17821068,-97.38887025&radius=5&start_date=08/07/2016 00:00&complete_date=08/06/2017 23:59
```

getVehicleReports:
```
/reports/vehicle?start=31.17821068,-97.38887025&radius=5
```

getDestinationReport:
```
/reports/max-and-min?start=31.17821068,-97.38887025&radius=5
```

## Documentation

OpenAPI 3
```
/v3/api-docs/
```

Swagger UI
```
/swagger-ui.html
```
