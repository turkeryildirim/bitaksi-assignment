package com.bitaksi.assignment.repository;

import com.bitaksi.assignment.model.entity.Trip;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Circle;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

/**
 * Repository for querying Trips.
 */
@Repository
public class TripRepository {

  @Autowired
  private MongoOperations operations;

  /**
   * Getting Trips by circle, startDate and completeDate.
   *
   * @param circle       area for query
   * @param startDate    startDate of trip gte startDate
   * @param completeDate completeDate of trip lte completeDate
   * @return List of Trips
   */
  public List<Trip> findByStartWithin(Circle circle, Date startDate, Date completeDate) {
    Criteria startCriteria = Criteria.where("start").withinSphere(circle);
    Query query = Query.query(startCriteria);

    Optional.ofNullable(startDate)
        .ifPresent((date) -> {
          Criteria startDateCriteria = Criteria.where("start_date").gte(date);
          query.addCriteria(startDateCriteria);
        });

    Optional.ofNullable(completeDate)
        .ifPresent((date) -> {
          Criteria completeDateCriteria = Criteria.where("complete_date").lte(date);
          query.addCriteria(completeDateCriteria);
        });


    return operations.find(query, Trip.class);
  }
}
