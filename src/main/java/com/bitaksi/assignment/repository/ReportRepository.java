package com.bitaksi.assignment.repository;

import com.bitaksi.assignment.model.entity.Trip;
import com.bitaksi.assignment.model.vo.DistanceReport;
import com.bitaksi.assignment.model.vo.VehicleReport;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Circle;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

/**
 * Repository for querying Trips and mapping report objects.
 */
@Repository
public class ReportRepository {

  @Autowired
  private MongoOperations operations;

  /**
   * Getting max and min travelled distance and mapping to DistanceReport.
   *
   * @param circle area for query
   * @return DistanceReport
   */
  public DistanceReport getDistanceReport(Circle circle) {
    Criteria startCriteria = Criteria.where("start").withinSphere(circle);

    Aggregation aggregation = Aggregation.newAggregation(
        Aggregation.match(startCriteria),
        Aggregation.group()
            .max("distance_travelled").as("maxTravelledDistance")
            .min("distance_travelled").as("minTravelledDistance")
    );

    return operations
        .aggregate(aggregation, Trip.class, DistanceReport.class)
        .getUniqueMappedResult();
  }

  /**
   * Getting count of trips about model years and mapping to VehicleReport.
   *
   * @param circle area for query
   * @return VehicleReport
   */
  public List<VehicleReport> getVehicleReport(Circle circle) {
    Criteria startCriteria = Criteria.where("start").withinSphere(circle);

    Aggregation aggregation = Aggregation.newAggregation(
        Aggregation.match(startCriteria),
        Aggregation.group("year")
            .count().as("tripCount")
            .first("year").as("modelYear")
    );

    return operations
        .aggregate(aggregation, Trip.class, VehicleReport.class)
        .getMappedResults();
  }
}
