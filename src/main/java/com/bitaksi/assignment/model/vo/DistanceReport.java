package com.bitaksi.assignment.model.vo;


import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Using for max and min travelled distance reporting.
 */
@AllArgsConstructor
@Getter
public class DistanceReport {
  int maxTravelledDistance;
  int minTravelledDistance;
}
