package com.bitaksi.assignment.model.vo;


import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Using for count of trips about model years reporting.
 */
@AllArgsConstructor
@Getter
public class VehicleReport {
  int modelYear;
  int tripCount;
}
