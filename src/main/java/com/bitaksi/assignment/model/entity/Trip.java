package com.bitaksi.assignment.model.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Entity of trips Document.
 */
@Document(collection = "trips")
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Trip {

  @Id
  String id = new ObjectId().toString();

  @Field("distance_travelled")
  int distanceTravelled;

  @Field("driver_rating")
  int driverRating;

  @Field("rider_rating")
  int riderRating;

  @Field("start_zip_code")
  String startZipCode;

  @Field("end_zip_code")
  String endZipCode;

  @Field("charity_id")
  String charityId;

  @Field("requested_car_category")
  String requestedCarCategory;

  @Field("free_credit_used")
  String freeCreditUsed;

  @Field("surge_factor")
  int surgeFactor;

  String color;

  String make;

  String model;

  int year;

  int rating;

  @Field("Date")
  String date;

  @Field("PRCP")
  int prcp;

  @Field("TMAX")
  int tmax;

  @Field("TMIN")
  int tmin;

  @Field("AWND")
  int awnd;

  @Field("GustSpeed2")
  int gustSpeed2;

  @Field("Fog")
  int fog;

  @Field("HeavyFog")
  int heavyPog;

  @Field("Thunder")
  int thunder;

  Point start;

  Point end;

  @Field("complete_date")
  Date completeDate;

  @Field("start_date")
  Date startDate;
}
