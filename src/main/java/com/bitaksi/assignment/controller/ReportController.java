package com.bitaksi.assignment.controller;

import com.bitaksi.assignment.model.vo.DistanceReport;
import com.bitaksi.assignment.model.vo.VehicleReport;
import com.bitaksi.assignment.repository.ReportRepository;
import java.util.List;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Circle;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller class for reports.
 */
@RestController
@Validated
@AllArgsConstructor
@RequestMapping("/reports")
public class ReportController {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  @Autowired
  private final ReportRepository reportRepository;

  /**
   * Receiving max and min travelled distance as a distance report.
   *
   * @param start          center coordinates for query
   * @param radius         radius of area
   * @param authentication for checking isAuthenticated and username
   * @return ResponseEntity with 200 OK status and DistanceReport on body.
   */
  @GetMapping("/max-and-min")
  @PreAuthorize("isAuthenticated()")
  public ResponseEntity<DistanceReport> getMaxAndMinDistance(
      @RequestParam("start") @NotNull Point start,
      @RequestParam("radius") @Min(1) @Max(999) int radius,
      Authentication authentication) {
    String username = authentication.getName();
    Circle circle = new Circle(start, new Distance(radius, Metrics.KILOMETERS));

    logger.info(username + " trying to get max and min travelled distance in given area."
        + " || Area => " + circle);

    DistanceReport report = reportRepository.getDistanceReport(circle);
    return ResponseEntity.status(HttpStatus.OK).body(report);
  }

  /**
   * Receiving count of trips about model years as a vehicle report list.
   *
   * @param start          center coordinates for query
   * @param radius         radius of area
   * @param authentication for checking isAuthenticated and username
   * @return ResponseEntity with 200 OK status and list of VehicleReport on body.
   */
  @GetMapping("/vehicle")
  @PreAuthorize("isAuthenticated()")
  public ResponseEntity<List<VehicleReport>> getVehicleReports(
      @RequestParam("start") @NotNull Point start,
      @RequestParam("radius") @Min(1) @Max(999) int radius,
      Authentication authentication) {
    String username = authentication.getName();
    Circle circle = new Circle(start, new Distance(radius, Metrics.KILOMETERS));

    logger.info(username + "trying to get count of trips about model years in given area."
        + " || Area => " + circle);

    List<VehicleReport> reports = reportRepository.getVehicleReport(circle);
    return ResponseEntity.status(HttpStatus.OK).body(reports);
  }
}
