package com.bitaksi.assignment.controller;

import com.bitaksi.assignment.model.entity.Trip;
import com.bitaksi.assignment.repository.TripRepository;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Circle;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * Controller class for trips.
 */
@RestController
@AllArgsConstructor
@RequestMapping("/trips")
public class TripController {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  @Autowired
  private final TripRepository tripRepository;


  /**
   * Receiving trips.
   *
   * @param start          center coordinates for query
   * @param radius         radius of area
   * @param startDate      startDate of trip gte startDate
   * @param completeDate   completeDate of trip lte completeDate
   * @param authentication for checking isAuthenticated and username
   * @return ResponseEntity with 200 OK status and list of Trip on body.
   */
  @GetMapping()
  @PreAuthorize("isAuthenticated()")
  public ResponseEntity<List<Trip>> getAllTrips(
      @RequestParam(value = "start") @NotNull Point start,
      @RequestParam("radius") @Min(1) @Max(999) int radius,
      @RequestParam(value = "start_date", required = false) Date startDate,
      @RequestParam(value = "complete_date", required = false) Date completeDate,
      Authentication authentication) {
    String username = authentication.getName();
    Circle circle = new Circle(start, new Distance(radius, Metrics.KILOMETERS));
    logger.info(username + " trying to get trips in given area. || Area => " + circle);

    List<Trip> trips = tripRepository.findByStartWithin(circle, startDate, completeDate);
    return ResponseEntity.status(HttpStatus.OK).body(trips);
  }
}
