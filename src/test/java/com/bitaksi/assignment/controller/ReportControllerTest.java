package com.bitaksi.assignment.controller;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.bitaksi.assignment.model.vo.DistanceReport;
import com.bitaksi.assignment.model.vo.VehicleReport;
import com.bitaksi.assignment.repository.ReportRepository;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.geo.Circle;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.security.core.Authentication;

@ExtendWith(MockitoExtension.class)
public class ReportControllerTest {

  @InjectMocks
  private ReportController reportController;

  @Mock
  private ReportRepository reportRepository;

  private Authentication authentication;

  private final Point point = new Point(9, 9);

  private final int radius = 9;

  private final Distance distance = new Distance(radius, Metrics.KILOMETERS);

  private final Circle circle = new Circle(point, distance);

  private final DistanceReport distanceReport = new DistanceReport(9, 1);

  private final VehicleReport vehicleReport = new VehicleReport(2020, 5);

  private final List<VehicleReport> vehicleReports = Collections.singletonList(vehicleReport);

  @BeforeEach
  public void beforeTest() {
    authentication = mock(Authentication.class);
  }

  @Test
  public void getMaxAndMinDistance_shouldReturnDistanceReports() {
    DistanceReport expected = distanceReport;

    when(reportRepository.getDistanceReport(circle)).thenReturn(distanceReport);

    DistanceReport actual = reportController
        .getMaxAndMinDistance(point, radius, authentication)
        .getBody();

    Assertions.assertEquals(expected, actual);
  }

  @Test
  public void getVehicleReport_shouldReturnVehicleReports() {
    List<VehicleReport> expected = vehicleReports;

    when(reportRepository.getVehicleReport(circle)).thenReturn(vehicleReports);

    List<VehicleReport> actual = reportController
        .getVehicleReports(point, radius, authentication)
        .getBody();

    Assertions.assertEquals(expected, actual);
  }
}
