package com.bitaksi.assignment.controller;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.bitaksi.assignment.model.entity.Trip;
import com.bitaksi.assignment.repository.TripRepository;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.geo.Circle;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.security.core.Authentication;

@ExtendWith(MockitoExtension.class)
public class TripControllerTest {

  @InjectMocks
  private TripController tripController;

  @Mock
  private TripRepository tripRepository;

  private Authentication authentication;

  private final Point point = new Point(9, 9);

  private final int radius = 9;

  private final Distance distance = new Distance(radius, Metrics.KILOMETERS);

  private final Circle circle = new Circle(point, distance);

  private final Date startDate = new Date();

  private final Date completeDate = new Date();

  private final Trip trip = new Trip();

  private final List<Trip> trips = Collections.singletonList(trip);

  @BeforeEach
  public void beforeTest() {
    authentication = mock(Authentication.class);
  }

  @Test
  public void getAllTrips_shouldReturnTrips() {
    List<Trip> expected = trips;

    when(tripRepository.findByStartWithin(circle, null, null)).thenReturn(trips);

    List<Trip> actual = tripController
        .getAllTrips(point, radius, null, null, authentication)
        .getBody();

    Assertions.assertEquals(expected, actual);
  }

  @Test
  public void getAllTrips_shouldReturnTrips_whenStartDateGiven() {
    List<Trip> expected = trips;

    when(tripRepository.findByStartWithin(circle, startDate, null)).thenReturn(trips);

    List<Trip> actual = tripController
        .getAllTrips(point, radius, startDate, null, authentication)
        .getBody();

    Assertions.assertEquals(expected, actual);
  }

  @Test
  public void getAllTrips_shouldReturnTrips_whenCompleteDateGiven() {
    List<Trip> expected = trips;

    when(tripRepository.findByStartWithin(circle, null, completeDate)).thenReturn(trips);

    List<Trip> actual = tripController
        .getAllTrips(point, radius, null, completeDate, authentication)
        .getBody();

    Assertions.assertEquals(expected, actual);
  }

  @Test
  public void getAllTrips_shouldReturnTrips_whenStarDateAndCompleteDateGiven() {
    List<Trip> expected = trips;

    when(tripRepository.findByStartWithin(circle, startDate, completeDate)).thenReturn(trips);

    List<Trip> actual = tripController
        .getAllTrips(point, radius, startDate, completeDate, authentication)
        .getBody();

    Assertions.assertEquals(expected, actual);
  }

}
